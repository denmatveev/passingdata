//
//  BooksViewController.swift
//  PassingData
//
//  Created by Денис Матвеев on 29/03/2020.
//  Copyright © 2020 Денис Матвеев. All rights reserved.
//

import UIKit

class BooksViewController: UITableViewController {
    var booksData = Books()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //navigationController?.navigationBar.prefersLargeTitles = true
        
        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller.
        guard let bookVC = segue.destination as? BookViewController,
            let selectedIndex = self.tableView.indexPathForSelectedRow?.row
            else {
                return
        }
        bookVC.book = booksData[selectedIndex]
        
        bookVC.updateBookClosure = { [weak self] updatedBook in
            self?.booksData.items[selectedIndex] = updatedBook
            self?.tableView.reloadData()
        }
    }
    
    @IBAction func saveBook(_ unwindSegue: UIStoryboardSegue) {
        if let addBookVC = unwindSegue.source as? AddBookViewController, let newBook = addBookVC.newBook {
            booksData.items.append(newBook)
            self.tableView.reloadData()
        }
    }
    
    @IBAction func cancelAdd(_ unwindSegue: UIStoryboardSegue) { }
    
    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return booksData.items.count
    }

    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cellIdentifier = "BooksTableViewCell"
        guard let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as? BooksTableViewCell else {
            fatalError("The dequeued cell is not an instance of \(cellIdentifier).")
        }
        
        // Configure the cell...
        let book = booksData[indexPath.row]
        cell.titleLabel.text = book.title
        cell.authorLabel.text = book.author ?? ""
        if let rating = book.rating {
            cell.ratingLabel.text = String("⭐️ \(rating)")
        } else {
            cell.ratingLabel.text = ""
        }
        
        return cell
    }
    

    /*
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
