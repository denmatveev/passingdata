//
//  AddBookViewControllerViewController.swift
//  PassingData
//
//  Created by Денис Матвеев on 29/03/2020.
//  Copyright © 2020 Денис Матвеев. All rights reserved.
//

import UIKit
import Combine

protocol BookViewControllerDelegate: AnyObject {
    func updateBook(_ book: Book)
}

class EditBookViewController: UIViewController {

    @IBOutlet weak var titleTextField: UITextField!
    @IBOutlet weak var authorNameTextField: UITextField!
    @IBOutlet weak var saveBarButtonItem: UIBarButtonItem!
    
    var book: Book!
    
    weak var delegate: BookViewControllerDelegate?
    
    @IBAction func textFieldNext(_ sender: UITextField) {
        let nextTag = sender.tag + 1

        if let nextResponder = sender.superview?.viewWithTag(nextTag) {
            nextResponder.becomeFirstResponder()
        } else {
            sender.resignFirstResponder()
        }
    }
    
    @IBAction func bookTitleEditChange(_ sender: UITextField, forEvent event: UIEvent) {
        saveBarButtonItem.isEnabled = sender.text != ""
    }
    
    @IBAction func save(_ sender: UIBarButtonItem) {
       if let title = titleTextField.text {
        let author = authorNameTextField.text
            let updatedBook = Book.init(title: title, author: author)
            delegate?.updateBook(updatedBook)
            self.dismiss(animated: true, completion: nil)
        }
    }
    
    @IBAction func cancel(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        titleTextField.becomeFirstResponder()
        titleTextField.text = book.title
        authorNameTextField.text = book.author
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
