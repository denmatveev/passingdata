//
//  BooksDataModel.swift
//  PassingData
//
//  Created by Денис Матвеев on 29/03/2020.
//  Copyright © 2020 Денис Матвеев. All rights reserved.
//

import Foundation

struct Book: Decodable {
    var title: String
    var author: String?
    var rating: Int?
    var review: String?
    
    init(title: String, author: String? = nil, rating: Int? = nil, review: String? = nil) {
        self.title = title
        self.author = author
        self.rating = rating
        self.review = review
    }
}

