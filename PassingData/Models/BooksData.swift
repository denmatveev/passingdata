//
//  BooksDataModel.swift
//  PassingData
//
//  Created by Денис Матвеев on 29/03/2020.
//  Copyright © 2020 Денис Матвеев. All rights reserved.
//

import Foundation
import Combine

struct Books {
    var items: [Book] = load("books.json")
    subscript(index: Int) -> Book {
        return items[index]
    }
}

func load<T: Decodable>(_ filename: String) -> T {
    let data: Data
    
    guard let file = Bundle.main.url(forResource: filename, withExtension: nil)
        else {
            fatalError("Couldn't find \(filename) in main bundle.")
    }
    
    do {
        data = try Data(contentsOf: file)
    } catch {
        fatalError("Couldn't load \(filename) from main bundle:\n\(error)")
    }
    
    do {
        let decoder = JSONDecoder()
        return try decoder.decode(T.self, from: data)
    } catch {
        fatalError("Couldn't parse \(filename) as \(T.self):\n\(error)")
    }
}
